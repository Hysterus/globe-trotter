﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using globe_trotter.backend.Models;
using globe_trotter.backend.ViewModels;
using System.Web.Mvc;
using System.Web.Http;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace globe_trotter.backend.Controllers
{
    public class CountriesController : ApiController
    {
        public IEnumerable<Country> GetAllCountries()
        {
            var allCountries = Country.Objects.All();

            return allCountries;
        }
    }
}