﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using globe_trotter.backend.ViewModels;
using System.Web.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace globe_trotter.backend.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public ActionResult Index()
        {
            return View("~/backend/Index.cshtml", new IndexViewModel());
        }
    }
}
