﻿namespace globe_trotter.backend.ViewModels
{
    public class IndexViewModel
    {
        public string JavascriptUrl { get; private set; } = "/Content/bundle.js";
        public string CssUrl { get; private set; } = "/Content/styles.css";
        //public string JavascriptUrl { get; private set; } = "http://localhost:8080/assets/bundle.js";
        //public string CssUrl { get; private set; } = "http://localhost:8080/assets/styles.css";
    }
}