﻿using globe_trotter.backend.Managers;

namespace globe_trotter.backend.Models
{
    public class Country : Model<XmlCountryManager>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string FlagURL { get; set; }
        public string InhabitantsCount { get; set; }
    }
}