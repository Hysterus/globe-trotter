﻿namespace globe_trotter.backend.Models
{
    public abstract class Model<TModelManager> where TModelManager :new()
    {
        public static TModelManager Objects;

        static Model()
        {
            Objects = new TModelManager();
        }
    }
}