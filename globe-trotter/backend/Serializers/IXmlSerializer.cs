﻿namespace globe_trotter.backend.Serializers
{
    internal interface IXmlSerializer<T>
    {
        T deserialize(string filename);
        bool serialize(string filename, T instance);
    }
}