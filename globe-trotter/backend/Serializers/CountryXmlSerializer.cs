﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using globe_trotter.backend.Models;

namespace globe_trotter.backend.Serializers
{
    public class CountryXmlSerializer : IXmlSerializer<List<Country>>
    {
        public List<Country> deserialize(string filename)
        {
            List<Country> countries = new List<Country>();

            using (var fs = new FileStream(filename, FileMode.Open))
            {
                var doc = XDocument.Load(fs);

                foreach (XElement node in doc.XPathSelectElements("countries/country"))
                {
                    countries.Add(new Country()
                    {
                        Name = node.Element("name").Value,
                        FlagURL = node.Element("flag").Value,
                        Description = node.Element("description").Value
                    });
                }
            }

            return countries;
        }

        public bool serialize(string filename, List<Country> instance)
        {
            throw new System.NotImplementedException();
        }
    }
}