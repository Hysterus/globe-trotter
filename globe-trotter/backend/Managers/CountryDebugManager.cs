using System.Collections.Generic;

namespace globe_trotter.backend.Models
{
    public class CountryDebugManager : IModelManager<Country>
    {
        public List<Country> All()
        {
            return new List<Country>
            {
                new Country() { Name="Poland", FlagURL = "http://flags.fmcdn.net/data/flags/w580/pl.png", Description = "Poland (Polish: Polska [?p?lska] ( listen)), officially the Republic of Poland (Polish: Rzeczpospolita Polska,[a] About this sound listen (help�info)), is a country in Central Europe,[10] situated between the Baltic Sea in the north and two mountain ranges (the Sudetes and Carpathian Mountains) in the south. Bordered by Germany to the west; the Czech Republic and Slovakia to the south;"},
                new Country() { Name="Panama", FlagURL = "http://flags.fmcdn.net/data/flags/w580/pa.png"},
                new Country() { Name="Colombia", FlagURL = "http://flags.fmcdn.net/data/flags/w580/co.png"},
            };
        }
    }
}