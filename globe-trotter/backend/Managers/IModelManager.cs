using System.Collections.Generic;

namespace globe_trotter.backend.Models
{
    public interface IModelManager<TModel>
    {
        List<TModel> All();
    }
}