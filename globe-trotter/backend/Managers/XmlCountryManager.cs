﻿using System.Collections.Generic;
using globe_trotter.backend.Models;
using globe_trotter.backend.Serializers;

namespace globe_trotter.backend.Managers
{
    public class XmlCountryManager : IModelManager<Country>
    {
        private IXmlSerializer<List<Country>> serializer = new CountryXmlSerializer();
        private string xmlFilePath = "countries.xml";

        public XmlCountryManager()
        {
            xmlFilePath = System.Web.HttpContext.Current.Server.MapPath("~/backend/countries.xml");
        }

        public List<Country> All()
        {
            return serializer.deserialize(xmlFilePath);
        }
    }
}