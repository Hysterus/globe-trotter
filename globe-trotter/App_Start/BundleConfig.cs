﻿using System.Web;
using System.Web.Optimization;

namespace globe_trotter
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/assets/bundle").Include(
                        "~/Content/bundle.js"));

            bundles.Add(new StyleBundle("~/assets/styles").Include(
                      "~/Content/styles.css"));
        }
    }
}
