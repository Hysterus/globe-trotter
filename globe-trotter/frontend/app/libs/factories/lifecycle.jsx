import React from "react"

export const withComponentWillMount = onMount => Component => (
    class Wrapper extends React.Component {
        componentWillMount() {
            onMount(this.props)
        }

        render() {
            return (
                <Component {...this.props}/>
            )
        }
    }

)