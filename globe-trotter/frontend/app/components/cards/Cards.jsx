import React from "react"

import "./Cards.scss"

export const Card = ({children, className = ""}) => (
    <div className={`card ${className} `}>
        {children}
    </div>
)

export const CardsList = ({children}) => <div className="cards-list">{children}</div>