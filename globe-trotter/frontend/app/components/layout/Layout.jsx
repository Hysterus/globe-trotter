import React from "react"
import {Link} from "react-router-dom"

import './layout.scss'

const Layout = ({children}) => (
    <div>
        <div className="banner">
            <Link to="/"><h1 className="banner__logo">globe<span className="colored">.</span>trotter</h1></Link>
        </div>
        <div className="page-container">
            {children}
        </div>
    </div>
)

export default Layout