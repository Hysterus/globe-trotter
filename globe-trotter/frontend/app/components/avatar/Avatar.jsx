import React from "react"

import "./avatar.scss"

const Avatar = ({src, className=""}) => (
    <div className={`avatar ${className}`}>
        <img src={src}/>
    </div>
)

export default Avatar;