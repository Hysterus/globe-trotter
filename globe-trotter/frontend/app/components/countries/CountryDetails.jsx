import React from "react"
import Avatar from "../avatar/Avatar";

const CountryDetails = ({data}) => (
    <div className="country-details">
        <div className="country-details__flag">
            <Avatar className="avatar--shadowed" src={data.FlagURL}/>
        </div>
        <div>
            <h2>{data.Name}</h2>
        </div>
        <div>
            <p>
                {data.Description}
            </p>
        </div>
    </div>
)

export default CountryDetails;