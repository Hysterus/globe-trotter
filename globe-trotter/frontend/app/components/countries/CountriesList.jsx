import React from "react"
import {CardsList, Card} from "../cards/Cards";
import Avatar from "../avatar/Avatar";
import {Link} from "react-router-dom";

import "./countries.scss"

export const CountriesList = ({children}) => (
    <CardsList>
        {children}
    </CardsList>
)

export const CountryRow = ({data}) => (
    <Card className="country-card">
        <Avatar className="avatar--small avatar--inline avatar--middle avatar--shadowed" src={data.FlagURL}/>
        <span className="country-name">
            <Link to={`/countries/${data.Name}`}>{data.Name}</Link>
        </span>
    </Card>
)