import axios from "axios"
import * as R from "ramda";

export const fetchCountries = dispatch => {
    dispatch({type: "FETCH_COUNTRIES_STARTED"})

    return axios.get("/api/countries")
        .then(R.prop('data'))
        .then(R.tap(data => dispatch({type: "FETCH_COUNTRIES_FINISHED", payload: data})))
        .catch(err => {
            dispatch({type: "FETCH_COUNTRIES_REJECTED", payload: err})
            throw err;
        })
}

const findCountryWithName = name => list => list.filter(R.compose(
    R.equals(name),
    R.prop("Name")
))

export const fetchCountry = name => (dispatch, getState) => {
    dispatch({type: "FETCH_COUNTRY_DETAILS_STARTED"})

    return new Promise((resolve, reject) => {
        const country = R.compose(
            R.head,
            findCountryWithName(name)
        )(getState().countries.list)

        if (country) {
            return resolve(country)
        }

        dispatch(fetchCountries)
            .then(findCountryWithName(name))
            .then(R.head)
            .then(country => {
                if(country) {
                    resolve(country)
                }
                else {
                    reject(`Countr ${name} not found`)
                }
            })
            .catch(err => reject(err))
    })
        .then(R.tap(data => dispatch({type: "FETCH_COUNTRY_DETAILS_FINISHED", payload: data})))
        .catch(err => {
            dispatch({type: "FETCH_COUNTRY_DETAILS_REJECTED", payload: "Country not found"})
            throw err;
        })
}