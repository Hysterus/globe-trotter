import React from "react"
import R from "ramda"
import Spinner from "../components/spinners/Spinner";
import {withComponentWillMount} from "../libs/factories/lifecycle";
import {connect} from "react-redux";
import {fetchCountry} from "../actions/countriesActions";
import CountryDetails from "../components/countries/CountryDetails";

const CountryDetailsPage = ({loading, country}) =>  {
    if(loading) {
        return <Spinner/>
    }

    return (
        <div>
            <h1>Country Details</h1>
            <CountryDetails data={country}/>
        </div>
    )
}

const mapStateToProps = ({activeCountry}) => R.pick(['loading', 'country'], activeCountry)
const mapDispatchToProps = dispatch => ({
    onEnter: R.compose(dispatch, fetchCountry)
})

export default R.compose(
    connect(mapStateToProps, mapDispatchToProps),
    withComponentWillMount(({match, onEnter}) => onEnter(match.params.name))
)(CountryDetailsPage)