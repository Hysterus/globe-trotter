import React from "react"
import R from "ramda"
import {connect} from "react-redux"
import {withComponentWillMount} from "../libs/factories/lifecycle";
import Spinner from "../components/spinners/Spinner";
import {fetchCountries} from "../actions/countriesActions";
import {CountriesList, CountryRow} from "../components/countries/CountriesList";

const CountriesPage = ({loading, list}) =>  {
    if(loading) {
        return <Spinner/>
    }

    return (
        <div>
            <h1>Cool Countries</h1>
            <CountriesList>
                {list.map(country => <CountryRow key={country.Name} data={country}/>)}
            </CountriesList>
        </div>
    )
}

const mapStateToProps = state => R.pick(['loading', 'list', 'error'], state.countries)
const mapDispatchToProps = dispatch => ({
    onMount: () => dispatch(fetchCountries)
})

export default R.compose(
    connect(mapStateToProps, mapDispatchToProps),
    withComponentWillMount(R.pipe(R.prop("onMount"), R.call))
)(CountriesPage)