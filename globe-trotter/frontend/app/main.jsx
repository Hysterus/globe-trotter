import React from 'react'
import ReactDOM from 'react-dom'

import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import {HashRouter as Router, Route, Link} from 'react-router-dom'
import logger from 'redux-logger'
import thunkMiddleware from 'redux-thunk'

import "./styles/main.scss"
import Layout from "./components/layout/Layout";
import CountriesPage from "./containers/CountriesPage";

import reducers from "./reducers"
import CountryDetailsPage from "./containers/CountryDetailsPage";

const middleware = [
    thunkMiddleware,
]

if(process.env.NODE_ENV !== 'production') {
    middleware.push(logger())
}

const store = createStore(
    combineReducers(reducers),
    applyMiddleware(...middleware)
)

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Layout>
                <Route exact path="/" component={CountriesPage}/>
                <Route path="/countries/:name" component={CountryDetailsPage} />
            </Layout>
        </Router>
    </Provider>,
    document.getElementById('app')
)