import {createReducer} from "../libs/reducers"

const defaultState = {
    list: [],
    loading: true,
    error: undefined
}

const handlers = {
    "FETCH_COUNTRIES_STARTED" : state => ({...state, loading: true}),
    "FETCH_COUNTRIES_FINISHED" : (state, action) => ({...state, loading: false, list: action.payload})
}

export default createReducer(defaultState, handlers)