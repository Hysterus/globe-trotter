import {createReducer} from "../libs/reducers"

const defaultState = {
    country: [],
    loading: true,
    error: undefined
}

const handlers = {
    "FETCH_COUNTRY_DETAILS_STARTED" : state => ({...state, loading: true}),
    "FETCH_COUNTRY_DETAILS_FINISHED" : (state, action) => ({...state, loading: false, country: action.payload}),
    "FETCH_COUNTRY_DETAILS_REJECTED" : (state, action) => ({...state, loading: false, error: action.payload})
}

export default createReducer(defaultState, handlers)