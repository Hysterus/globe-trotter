import countriesReducer from "./countriesReducer"
import countryDetailsReducer from "./countryDetailsReducer"

export default {
    countries: countriesReducer,
    activeCountry: countryDetailsReducer
}